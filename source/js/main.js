'use strict';
//= ../node_modules/jquery/dist/jquery.js
//= ../node_modules/popper.js/dist/umd/popper.js
//= template/slick.min.js
//= template/nice-select.min.js
//= template/countdown.jquery.min.js
//= template/lightbox.min.js
//= template/custom.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js
//= ../node_modules/lazysizes/lazysizes.js

//= library/jquery-ui.js


$(document).ready(function () {

	$('#standard-price').slider({
		orientation: "horizontal",
		range: "min",
		max: 100000,
		value: 50000,
		slide: function( event, ui ) {
			$( "#price_1" ).val( numberWithSpaces( $( "#standard-price" ).slider("value") ) + " ₽");
		}
	});
	$( "#price_1" ).val( numberWithSpaces( $( "#standard-price" ).slider("value") ) + " ₽");



	$(".quantity__input").on("change", function () {
		console.log($(this).val());
	});

});

function numberWithSpaces(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}
